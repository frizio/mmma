
%%
clear all;
close all;
clc;

%%
% Set this values to try different conditions and alghorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diffusion coefficient
D = 1;
% SPACE
xi = 0; xf = 1;
N = 2^13; %10000;
% TIME
ti = 0; tf = 0.1;
M = 100;
% Boundary Condition values
bi = 0; bf = 0;
% METHOD
MTH = 'im';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
xlinear = linspace(xi, xf, N);
dx = (xf-xi) / (N-1);

tlinear = linspace(ti, tf, M);
dt = (tf-ti) / (M-1);

r = dt/dx^2;

Dr = D*r;

disp(['Numeric Method: ', MTH]);
s = ['Space-steps:N=', num2str(N),', Time-steps:M=', num2str(M), ...
     ', dx=', num2str(dx), ', dt=', num2str(dt), ...
     ', r=', num2str(r), ', D=', num2str(D)];
disp(s);

[T, X] = meshgrid(tlinear, xlinear);
U = zeros(N, M);

%%
tmp = sin(2*pi*xlinear);
U(:,1) = tmp;

% BUONDARY CONDITIONS
U(1,:) = bi;
U(end,:) = bf;

%%
s = ['Numeric Method: IMPLICIT. ', s];
C = sparse(diag(ones(1,N)*1+2*Dr)) + ...
    sparse(diag(-ones(1,N-1)*Dr, 1)) + ...
    sparse(diag(-ones(1,N-1)*Dr, -1));
Cinv = inv(C);
for m=2 : M
    tmp= Cinv * U(:,m-1);
    tmp(1) = bi;
    tmp(end) = bf;
    U(:,m) = tmp;
end

%%
Utrue = U;
Ntrue  = N;

steps = [2, 4, 8, 16, 32, 64];
ns = length(steps);

U_vector = cell(1, ns);

dx_vector = zeros(1, ns);
E_vector = zeros(1, ns);
N_vector = zeros(1, ns);

for i=1 : length(steps)
    N = Ntrue / steps(i);
    N_vector(i) = N;
    xlinear = linspace(xi, xf, N);
    dx = (xf-xi) / (N-1);
    dx_vector(i) = dx;
    r = dt/dx^2;
    
    [T, X] = meshgrid(tlinear, xlinear);
    U = zeros(N, M);
    
    % INITIAL CONDITIONS
    tmp = sin(2*pi*xlinear);
    U(:,1) = tmp;
    % BUONDARY CONDITIONS
    U(1,:) = bi;
    U(end,:) = bf;
    
    C = sparse(diag(ones(1,N)*1+2*Dr)) + ...
    sparse(diag(-ones(1,N-1)*Dr, 1)) + ...
    sparse(diag(-ones(1,N-1)*Dr, -1));
    Cinv = inv(C);
    for m=2 : M
        tmp= Cinv * U(:,m-1);
        tmp(1) = bi;
        tmp(end) = bf;
        U(:,m) = tmp;
    end
    
    U_vector{i} = U;
    
    UtrueCurrent = Utrue([1:steps(i):end], :);
    
    % fisso uno step spaziale e calcolo l'errore su tutti i tempi
    Ecurrent = max(abs(U(end,:) - UtrueCurrent(end,:)));
    
    E_vector(i) = Ecurrent;

end


%%
figure
loglog(dx_vector, E_vector, '*')
hold on;
grid on;
loglog(dx_vector, E_vector, '-r')
xlabel('dx (space-steps)')
ylabel('E (error - sup measure)')
title('Space connvergence')

%(log(E_vector(2) - log(E_vector(1)))) / (log(dx_vector(2)) - log(dx_vector(1)));

p_vector = zeros(1, ns);
for i=1 : ns-1
    p_vector(i) = (log(E_vector(i+1) - log(E_vector(i)))) / ...
           (log(dx_vector(i+1)) - log(dx_vector(i)));
end

disp('N')
disp(N_vector)
disp('dx')
disp(dx_vector)
disp('E')
disp(E_vector)
disp('p')
disp(p_vector)
