% Script : DIFFUSION EQUATION
% testing numeric methods
% 
% ...
% Maurizio La Rocca, [mrz.larocca-AT-gmail.com]
% Master's Course : Metodi e modelli matematici per le applicazioni.

%%
clear all;
close all;
clc;

%%
% Set this values to try different conditions and alghorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diffusion coefficient
D = 1;
% SPACE
xi = 0; xf = 1;
N = 500;
% TIME
ti = 0; tf = 0.1;
M = 100;
% Initial Condition function
f = 's';
% Boundary Condition values
bi = 0; bf = 0;
% METHOD
MTH = 'cnm';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
xlinear = linspace(xi, xf, N);
dx = (xf-xi) / (N-1);

tlinear = linspace(ti, tf, M);
dt = (tf-ti) / (M-1);

r = dt/dx^2;

Dr = D*r;

disp(['Numeric Method: ', MTH]);
s = ['Space-steps:N=', num2str(N),', Time-steps:M=', num2str(M), ...
     ', dx=', num2str(dx), ', dt=', num2str(dt), ...
     ', r=', num2str(r), ', D=', num2str(D)];
disp(s);

[T, X] = meshgrid(tlinear, xlinear);
U = zeros(N, M);

%%
% INITIAL CONDITION
switch f
    case 's'                        % sine
        tmp = sin(2*pi*xlinear);
    case 's2'                       % sine + 4
        tmp = 4 + sin(2*pi*xlinear);
    case 'c'                        % cosine
        tmp = cos(2*pi*xlinear);
    case 'gn'                       % normal gaussian
        tmp = 1/(std(xlinear)*sqrt(2*pi)) .* ... 
              exp( -((xlinear-mean(xlinear)).^2)./(2*var(xlinear)) );
    case 'r'                        % random
        tmp = rand(N, 1);
    case 'n'                        % random normal (mean, devStd)
        tmp = random('Normal', 7, 3, N, 1);
    case 'h'                        % heavyside
        k = 2;
        tmp = [zeros(1, floor(N/k)), ones(1, floor(N - N/k))];
    otherwise
        disp('WARNING : Not definied Function')
        tmp = xlinear;
end
U(:,1) = tmp;

% BUONDARY CONDITIONS
U(1,:) = bi;
U(end,:) = bf;

%%
switch MTH
    
    case 'itr'
        s = ['Numeric Method: SPACE ITERATIVE. ', s];
        for m=2 : M
            for j=2 : N-1
                U(j,m) = Dr*U(j-1,m-1) + (1-2*Dr)*U(j,m-1) + Dr*U(j+1,m-1);
            end
        end
        
    case 'em' 
        s = ['Numeric Method: EXPLICIT. ', s];
        B = sparse(diag(ones(1,N)*1-2*Dr)) + ...
            sparse(diag(ones(1,N-1)*Dr, 1)) + ...
            sparse(diag(ones(1,N-1)*Dr, -1));
        for m=2 : M
            tmp= B * U(:,m-1);
            tmp(1) = bi;
            tmp(end) = bf;
            U(:,m) = tmp;
        end
        
    case 'im'
        s = ['Numeric Method: IMPLICIT. ', s];
        C = sparse(diag(ones(1,N)*1+2*Dr)) + ...
            sparse(diag(-ones(1,N-1)*Dr, 1)) + ...
            sparse(diag(-ones(1,N-1)*Dr, -1));
        Cinv = inv(C);
        for m=2 : M
            tmp= Cinv * U(:,m-1);
            tmp(1) = bi;
            tmp(end) = bf;
            U(:,m) = tmp;
        end
        
    case 'cnm'
        s = ['Numeric Method: CRANK-NICOLSON. ', s];
        B = sparse(diag(ones(1,N)*1-2*Dr)) + ...
            sparse(diag(ones(1,N-1)*Dr, 1)) + ...
            sparse(diag(ones(1,N-1)*Dr, -1));
        C = sparse(diag(ones(1,N)*1+2*r)) + ...
            sparse(diag(-ones(1,N-1)*r, 1)) + ...
            sparse(diag(-ones(1,N-1)*r, -1));
        Cinv = inv(C);
        for m=2 : M
            tmp= Cinv * B * U(:,m-1);
            tmp(1) = bi;
            tmp(end) = bf;
            U(:,m) = tmp;
        end
        
    otherwise
        disp('Numeric method not definied')
end


%%
% Plot
hf = figure('Name', 'Heat Equation');
hold on;
grid on; 

surf(X, T, U)
shading interp

xlabel('X (Space)')

ylabel('T (Time)')
zlabel('U (Heat)')
title(s);

%%
j = floor(M/2);
i = floor(N/2);
figure; 
subplot(2,3,1)
plot(xlinear, U(:,1))
xlabel('X (Space)')
ylabel('Q (Diffusion)')
title('Initial Time')
subplot(2,3,2)
plot(xlinear, U(:,j))
xlabel('X (Space)')
ylabel('Q (Diffusion)')
title('Intermediate Time')
subplot(2,3,3)
plot(xlinear, U(:,end))
xlabel('X (Space)')
ylabel('Q (Diffusion)')
title('Final Time')

subplot(2,3,4)
plot(tlinear, U(1,:));
xlabel('T (Time)')
ylabel('Q (Diffusion)')
title('Boundary One')
subplot(2,3,5)
plot(tlinear, U(i,:));
xlabel('T (Time)')
ylabel('Q (Diffusion)')
title('Middle point')
subplot(2,3,6)
plot(tlinear, U(end,:));
xlabel('T (Time)')
ylabel('Q (Diffusion)')
title('Boundary Two')
